<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Route for normal user
Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('search', 'HomeController@search')->name('search');
    Route::get('/book/{id}', 'HomeController@show')->name('viewbook');

    Route::get('/my-trade', 'MyTradeController@index')->name('mytrade');
    Route::get('/add-trade', 'MyTradeController@create')->name('addtrade');
    Route::get('/my-trade/{id}', 'MyTradeController@show')->name('viewmytrade');
    Route::post('trade', 'MyTradeController@store')->name('storetrade');
    Route::get('/my-trade/{id}/edit', 'MyTradeController@edit')->name('editmytrade');
    Route::post('/my-trade/{id}/update', 'MyTradeController@update')->name('updatemytrade');
    Route::get('/my-trade/{id}/delete', 'MyTradeController@destroy')->name('delmytrade');
    Route::get('/my-trade/{id}/submit', 'MyTradeController@submit')->name('submitoffer');

    Route::get('/my-offer', 'MyOfferController@index')->name('myoffer');
    Route::get('/offer/{id}', 'MyOfferController@create')->name('addoffer');
    Route::post('offer', 'MyOfferController@store')->name('storeoffer');
    Route::get('/offer/{id}/delete', 'MyOfferController@destroy')->name('deloffer');


    //Route for admin
    Route::group(['prefix' => 'admin'], function(){
        Route::group(['middleware' => ['admin']], function(){
            Route::get('/book-list', 'admin\BookController@index')->name('booklist');
            Route::get('/add-book', 'admin\BookController@create')->name('addbook');
            Route::post('add-book', 'admin\BookController@store')->name('storebook');
            Route::get('/book/{id}', 'admin\BookController@show')->name('showbook');
            Route::get('/book/{id}/edit', 'admin\BookController@edit')->name('editbook');
            Route::post('/book/{id}/update', 'admin\BookController@update')->name('updatebook');
            Route::get('/book/{id}/delete', 'admin\BookController@destroy')->name('delbook');

            Route::get('/trade-list', 'admin\TradeController@index')->name('tradelist');
            Route::get('/trade/{id}', 'admin\TradeController@show')->name('showtrade');
            Route::get('/trade/{id}/edit', 'admin\TradeController@edit')->name('edittrade');
            Route::post('/trade/{id}/update', 'admin\TradeController@update')->name('updatetrade');
            Route::get('/trade/{id}/delete', 'admin\TradeController@destroy')->name('deltrade');
        });
    });
});

Route::get('logout', 'LoginController@logout')->name('logout');
