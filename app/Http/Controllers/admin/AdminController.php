<?php

namespace App\Http\Controllers\admin;

use App\Book;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        $books = Book::OrderBy('created_at', 'desc')->get();
        return view('admin/booklist')->with('books', $books);
    }
}
