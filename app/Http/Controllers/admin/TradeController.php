<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Offer;
use App\Trade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TradeController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $trades = DB::table('trades')
            ->join('books','trades.trade_isbn','=','books.isbn')
            ->join('users','trades.trade_user_id','=','users.id')
            ->select('trades.*', 'books.book_name', 'users.name')
            ->get();
        return view('admin/tradelist')->with('trades', $trades);
    }

    public function show($id)
    {
        $trades = DB::table('trades')
            ->join('books','trades.trade_isbn','=','books.isbn')
            ->join('users','trades.trade_user_id','=','users.id')
            ->select('trades.*', 'books.book_name', 'users.name')
            ->where('trades.id', '=', $id)
            ->get();

        foreach ($trades as $trade){
            if ($trade->trade_status == 'complete'){
                $offers = DB::table('offers')
                    ->join('books','offers.offer_isbn','=','books.isbn')
                    ->join('users', 'offers.offer_user_id', '=', 'users.id')
                    ->select('offers.*', 'books.book_name as book_name', 'users.name as user_name')
                    ->get();
                return view('admin/tradedetail')->with('trades', $trades)->with('offers', $offers);
            }
        }
        return view('admin/tradedetail')->with('trades', $trades);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $trade = Trade::find($id);
        $trade->delete();
        return redirect('admin/trade-list')->with('success', 'Deleted ID : '.$trade->id);
    }
}
