<?php

namespace App\Http\Controllers\admin;

use App\Book;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::OrderBy('created_at', 'desc')->get();
        return view('admin/booklist')->with('books', $books);
    }

    public function create()
    {
        return view('admin/addbook');
    }

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'isbn'=>'required',
                'book_name'=>'required',
                'book_logline'=>'required',
                'book_image'=>'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'writer'=>'required',
            ]
        );
        $file_name = time().'.'.$request->book_image->extension();
        $file = $request->file('book_image');
        $file->move(public_path('uploads'),$file_name);

        $book = new Book();
        $book->isbn = $request->input('isbn');
        $book->book_name = $request->input('book_name');
        $book->book_logline = $request->input('book_logline');
        $book->book_image = "uploads/".$file_name;
        $book->writer = $request->input('writer');
        $book->save();

        return redirect('admin/add-book')->with('success', 'เพิ่มหนังสือ "'.$book->book_name.'" สำเร็จ');
    }

    public function show($id)
    {
        $book = Book::find($id);
        return view('admin/bookdetail')->with('book', $book);
    }

    public function edit($id)
    {
        $book = Book::find($id);
        return view('admin/editbook')->with('book', $book);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'isbn'=>'required',
                'book_name'=>'required',
                'book_logline'=>'required',
                'writer'=>'required',
            ]
        );

        $book = Book::find($id);
        $book->isbn = $request->input('isbn');
        $book->book_name = $request->input('book_name');
        $book->book_logline = $request->input('book_logline');

        if($request->file('book_image') !== null){
            $file_name = time().'.'.$request->book_image->extension();
            $file = $request->file('book_image');
            $file->move(public_path('uploads'),$file_name);
            $book->book_image = "uploads/".$file_name;
        }
        else {
            $book->book_image = $book->book_image;
        }

        $book->writer = $request->input('writer');
        $book->save();

        return redirect('admin/book-list')->with('success', 'แก้ไข "'.$book->book_name.'" เรียบร้อย');
    }

    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect('admin/book-list')->with('success', 'Deleted ISBN #'.$book->isbn);
    }
}
