<?php

namespace App\Http\Controllers;

use App\Book;
use App\Search;
use App\Trade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->isAdmin()) {
            return redirect('admin/book-list');
        } else {
            $books = Book::OrderBy('created_at', 'desc')->get();
            $trades = DB::table('trades')
                ->leftJoin('books','trades.trade_isbn','=','books.isbn')
                ->leftJoin('users','trades.trade_user_id','=','users.id')
                ->select('trades.*', 'books.book_name', 'users.name')
                ->where('trades.trade_status', '=', 'incomplete')
                ->orderBy('created_at', 'desc')
                ->get();
            return view('home')
                ->with('books', $books)
                ->with('trades', $trades);
        }
    }

    public function show($id)
    {
        $book = Book::find($id);
        $trades = DB::table('trades')
            ->leftJoin('books','trades.trade_isbn','=','books.isbn')
            ->leftJoin('users','trades.trade_user_id','=','users.id')
            ->select('trades.*', 'books.book_name', 'users.name')
            ->where('trades.trade_status', '=', 'incomplete')
            ->where('trade_isbn', '=', $book->isbn)
            ->get();
        return view('viewbook')->with('book', $book)->with('trades', $trades);
    }

    public function search(Request $request){
        $search = $request->input('searchname');
        $books = Book::where('book_name', 'like', '%'.$search.'%')->get();
        $trades = DB::table('trades')
            ->leftJoin('books','trades.trade_isbn','=','books.isbn')
            ->leftJoin('users','trades.trade_user_id','=','users.id')
            ->select('trades.*', 'books.book_name', 'users.name')
            ->where('trades.trade_status', '=', 'incomplete')
            ->where('books.book_name', 'like', '%'.$search.'%')
            ->orderBy('created_at', 'desc')
            ->get();
        return view('home')->with('books', $books)->with('trades', $trades);
    }
}
