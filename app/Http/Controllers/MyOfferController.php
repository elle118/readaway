<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Trade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MyOfferController extends Controller
{

    public function index()
    {
        return view('offer');
    }

    public function create($id)
    {
        $trades = DB::table('trades')
            ->leftJoin('books','trades.trade_isbn','=','books.isbn')
            ->leftJoin('users','trades.trade_user_id','=','users.id')
            ->select('trades.*', 'books.book_name', 'users.name')
            ->where('trades.id', '=', $id)
            ->get();
        return view('addoffer')->with('trades', $trades);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'isbn'=>'required',
            'message'=>'required',
            'image'=>'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $file_name = time().'.'.$request->image->extension();
        $file = $request->file('image');
        $file->move(public_path('uploads'),$file_name);

        $offer = new Offer();
        $offer->offer_user_id = Auth::user()->id;
        $offer->trade_id = $request->input('trade_id');
        $offer->offer_isbn = $request->input('isbn');
        $offer->offer_message = $request->input('message');
        $offer->offer_image = "uploads/".$file_name;
        $offer->offer_status = 'incomplete';
        $offer->save();

        return redirect('/')->with('success', 'ส่งคำขอสำเร็จ!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $offer = Offer::find($id);
        $offer->delete();
        return redirect()->back()->with('success', 'ปฏิเสธการแลกเปลี่ยน ISBN : '.$offer->offer_isbn.' สำเร็จ');
    }
}
