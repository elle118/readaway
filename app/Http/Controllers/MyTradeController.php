<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Trade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MyTradeController extends Controller
{
    public function index()
    {
        $user_id = Auth::user()->id;
        $trades = DB::table('trades')
            ->join('books','trades.trade_isbn','=','books.isbn')
            ->join('users','trades.trade_user_id','=','users.id')
            ->select('trades.*', 'books.book_name', 'users.name')
            ->where('trades.trade_user_id', '=', $user_id)
            ->orderBy('created_at', 'desc')
            ->get();
        return view('trade')->with('trades', $trades);
    }

    public function create()
    {
        return view('addtrade');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'isbn'=>'required',
            'message'=>'required',
            'image'=>'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $file_name = time().'.'.$request->image->extension();
        $file = $request->file('image');
        $file->move(public_path('uploads'),$file_name);

        $trade = new Trade();
        $trade->trade_user_id = Auth::user()->id;
        $trade->trade_isbn = $request->input('isbn');
        $trade->trade_caption = $request->input('message');
        $trade->trade_image = "uploads/".$file_name;
        $trade->trade_status = "incomplete";
        $trade->offer_id = 0;
        $trade->save();

        return redirect()->back()->with('success', 'เพิ่มรายการใหม่สำเร็จ!');
    }

    public function show($id)
    {
        $trades = DB::table('trades')
            ->join('books','trades.trade_isbn','=','books.isbn')
            ->join('users','trades.trade_user_id','=','users.id')
            ->select('trades.*', 'books.book_name', 'users.name')
            ->where('trades.id', '=', $id)
            ->get();

        foreach ($trades as $trade){
            if ($trade->trade_status == 'complete'){
                $offers = DB::table('offers')
                    ->join('books','offers.offer_isbn','=','books.isbn')
                    ->join('users', 'offers.offer_user_id', '=', 'users.id')
                    ->select('offers.*', 'books.book_name as book_name', 'users.name as user_name')
                    ->where('offers.id', '=', $trade->offer_id)
                    ->get();
                return view('viewmytrade')->with('trades', $trades)->with('offers', $offers);
            }
            else {
                $offers = DB::table('offers')
                    ->join('books','offers.offer_isbn','=','books.isbn')
                    ->join('users', 'offers.offer_user_id', '=', 'users.id')
                    ->select('offers.*', 'books.book_name as book_name', 'users.name as user_name')
                    ->where('offers.trade_id', '=', $id)
                    ->where('offers.offer_status', '=', 'incomplete')
                    ->get();
                return view('viewmytrade')->with('trades', $trades)->with('offers', $offers);
            }
        }
        return view('viewmytrade')->with('trades', $trades);
    }

    public function edit($id)
    {
        $trade = Trade::find($id);
        return view('editmytrade')->with('trade', $trade);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'message'=>'required',
            ]
        );

        $trade = Trade::find($id);
        $trade->trade_caption = $request->input('message');

        if($request->file('image') !== null){
            $file_name = time().'.'.$request->image->extension();
            $file = $request->file('image');
            $file->move(public_path('uploads'),$file_name);
            $trade->trade_image = "uploads/".$file_name;
        }
        else {
            $trade->trade_image = $trade->trade_image;
        }

        $trade->save();

        return redirect('/my-trade')->with('success', 'แก้ไขรายการแลกเปลี่ยน #'.$trade->trade_isbn.' เรียบร้อย');
    }

    public function destroy($id)
    {
        $trade = Trade::find($id);
        $trade->delete();
        return redirect('/my-trade')->with('success', 'ลบรายการ ISBN #'.$trade->trade_isbn.' สำเร็จ');
    }

    public function submit(Request $request, $id){
        $offer = Offer::find($request->get('offer_id'));
        $trade = Trade::find($id);
        $offer->offer_status = 'complete';
        $offer->save();

        $trade->trade_status = 'complete';
        $trade->offer_id = $offer->id;
        $trade->save();

        return redirect()->back()->with('success', 'แลกเปลี่ยนสำเร็จ!');
    }
}

