<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $table = 'books';
    protected $primaryKey = 'id';
    protected $fillable = 'book_name';
}
