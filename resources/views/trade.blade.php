@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="mt-4 mb-4">
            <h2 class="m-1">
                รายการแลกเปลี่ยนของคุณ
            </h2>
        </div>
        @if(count($trades) > 0)
            <div class="row row-cols-1 row-cols-md-3">
                @foreach($trades as $trade)
                    <div class="col mb-4">
                        <div class="card shadow-sm">
                            <div class="card-body pb-2">
                                <a class="text-dark" href="{{ route('viewmytrade', $trade->id) }}">
                                    <h5 class="card-title m-0">{{$trade->book_name}}</h5>
                                </a>
                                <span class="text-black-50">ISBN : {{$trade->trade_isbn}}</span>
                            </div>
                            <img src="{{$trade->trade_image}}" class="object-cover" height="200" alt="{{$trade->book_name}}">
                            <div class="card-body">
                                @if($trade->trade_status == 'incomplete')
                                    <span class="text-black-50">{{$trade->created_at}}
                                        <p class="card-text text-red">{{$trade->trade_status}}</p>
                                    </span>
                                    <p class="pt-3 pb-4">{{$trade->trade_caption}}</p>
                                    <div class="float-right">
                                        <a href="{{ route('delmytrade', $trade->id) }}" class="btn btn-outline-red" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                                            <i class="far fa-trash-alt"></i> ลบ
                                        </a>
                                        <a href="{{ route('editmytrade', $trade->id) }}" class="btn btn-outline-warning">
                                            <i class="far fa-edit"></i> แก้ไข
                                        </a>
                                    </div>
                                @else
                                    <span class="text-black-50">{{$trade->created_at}}
                                        <p class="card-text text-success">{{$trade->trade_status}}</p>
                                    </span>
                                    <p class="pt-3 pb-4">{{$trade->trade_caption}}</p>
                                    <div class="float-right">
                                        <button class="btn btn-outline-secondary" disabled>
                                            <i class="far fa-trash-alt"></i> ลบ
                                        </button>
                                        <button class="btn btn-outline-secondary" disabled>
                                            <i class="far fa-edit"></i> แก้ไข
                                        </button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="text-center p-4 m-auto">
                <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
                <a href="{{route('addbook')}}">
                    <button class="btn btn-lg btn-outline-red font-weight-bold p-3 pl-5 pr-5"><i class="fas fa-plus"></i> เพิ่มรายการแรก</button>
                </a>
            </div>
            <div class="text-center p-5 m-5">
            </div>
        @endif
    </div>
    <script>
        $("#trade-list").hide();
        $("#btn-book-list").click(function(){
            $("#btn-book-list").addClass('btn-red');
            $("#btn-trade-list").removeClass('btn-red');
            $("#book-list").show();
            $("#trade-list").hide();
        });
        $("#btn-trade-list").click(function(){
            $("#btn-book-list").removeClass('btn-red');
            $("#btn-trade-list").addClass('btn-red');
            $("#book-list").hide();
            $("#trade-list").show();
        });
    </script>
@endsection
