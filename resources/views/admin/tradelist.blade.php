@extends('layouts.app')

@section('content')
    <div class="container">
        @if(count($trades) > 0)
            <div class="mt-4 mb-4">
                <h2>
                    รายการแลกเปลี่ยน <span class="text-red" style="font-size: 16px;">ทั้งหมด {{count($trades)}} รายการ</span>
                </h2>
            </div>
            <div class="card-body bg-white rounded shadow-sm overflow-auto pr-5 pl-5">
                <div class="row">
                    <div class="col-sm-1 m-auto">ID</div>
                    <div class="col m-auto">ISBN</div>
                    <div class="col m-auto">ชื่อหนังสือ</div>
                    <div class="col m-auto">เจ้าของ</div>
                    <div class="col m-auto">สถานะ</div>
                    <div class="col-sm-1 m-auto">Action</div>
                </div><hr>
                @foreach($trades as $trade)
                    <div class="row pt-3 pb-3">
                        <a class="col-sm-1 m-auto text-red font-weight-bold" href="{{ route('showtrade', $trade->id) }}">
                            {{$trade->id}}
                        </a>
                        <div class="col m-auto">{{$trade->trade_isbn}}</div>
                        <div class="col m-auto">{{$trade->book_name}}</div>
                        <div class="col m-auto">{{$trade->name}}</div>
                        @if($trade->trade_status == 'incomplete')
                            <div class="col m-auto text-red">{{$trade->trade_status}}</div>
                        @else
                            <div class="col m-auto text-success">{{$trade->trade_status}}</div>
                        @endif

                        <div class="col-sm-1 m-auto">
                            <a href="{{ route('deltrade', $trade->id) }}" class="btn btn-outline-red btn-rounded" onclick="return confirm('ยืนยันการลบรายการแลกเปลี่ยน?');">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="text-center p-5 m-5">
                <h1 class="pb-3 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน :(</h1>
            </div>
        @endif
    </div>
@endsection
