@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($trades as $trade)
        <div class="row overflow-auto">
            <div class="col mt-4 mb-4 float-left">
                <h2>
                    ID : {{$trade->id}}
                    <a class="btn btn-red float-right" href="{{ route('tradelist') }}">
                        <i class="fas fa-chevron-left"></i> ย้อนกลับ
                    </a>
                </h2>
            </div>
        </div>
        <div class="bg-white rounded shadow-sm">
            <div class="row">
                <div class="col-auto">
                    <img class="preview-img-2" src="/{{$trade->trade_image}}">
                </div>
                <div class="col pt-3 position-relative pr-5">
                    <h2>{{$trade->name}}</h2>
                    <p>
                        <h5 class="p-0 m-0">{{$trade->book_name}}</h5>
                        <span class="text-black-50">ISBN : {{$trade->trade_isbn}}<br>{{$trade->created_at}}</span>
                    </p>
                    <p>สถานะ :
                    @if($trade->trade_status == 'incomplete')<span class="text-red">
                    @else<span class="text-success">@endif
                        {{$trade->trade_status}}</span>
                    </p>
                    <p class="text-font">{{$trade->trade_caption}}</p>
                    @if($trade->offer_id != 0)
                        @foreach($offers as $offer)
                            @if($trade->offer_id == $offer->id)
                                <p class="position-absolute pos-bottom-left">
                                    หนังสือที่แลก : <span class="text-success">{{$offer->book_name}}</span><br>
                                    ผู้แลกเปลี่ยน : <span class="text-success">{{$offer->user_name}}</span>
                                </p>
                            @endif
                        @endforeach
                    @else
                        <p class="position-absolute pos-bottom-left">
                            หนังสือที่แลก : <span class="text-red">incomplete</span><br>
                            ผู้แลกเปลี่ยน : <span class="text-red">incomplete</span>
                        </p>
                    @endif
                    <div class="position-absolute pos-bottom-right">
                        <a href="{{ route('deltrade', $trade->id) }}" class="btn btn-outline-red" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                            <i class="far fa-trash-alt"></i> ลบ
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
