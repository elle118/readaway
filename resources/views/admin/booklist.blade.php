@extends('layouts.app')

@section('content')
    <div class="container">
        @if(count($books) > 0)
            <a href="{{route('addbook')}}">
                <button id="addbook" class="btn btn-red p-0"><i class="fas fa-plus"></i></button>
            </a>
            <div class="mt-4 mb-4">
                <h2>
                    รายการหนังสือ <span class="text-red" style="font-size: 16px;">ทั้งหมด {{count($books)}} เล่ม</span>
                </h2>
            </div>
            <div class="card-body bg-white rounded shadow-sm overflow-auto pr-5 pl-5">
                <div class="row">
                    <div class="col-sm-1 m-auto"></div>
                    <div class="col-md-2 m-auto">ISBN</div>
                    <div class="col m-auto">ชื่อหนังสือ</div>
                    <div class="col m-auto">นักเขียน</div>
                    <div class="col-sm-2 m-auto">Action</div>
                </div><hr>
                @foreach($books as $book)
                    <div class="row pt-3 pb-3">
                        <div class="col-sm-1">
                            <img class="img-list rounded-lg mr-3" src="/{{$book->book_image}}" alt="{{$book->book_name}}">
                        </div>
                        <a class="col-md-2 m-auto text-dark font-weight-bold" href="{{ route('showbook', $book->id) }}">
                            {{$book->isbn}}
                        </a>
                        <div class="col m-auto">{{$book->book_name}}</div>
                        <div class="col m-auto">{{$book->writer}}</div>
                        <div class="col-sm-2 m-auto">
                            <a href="{{ route('editbook', $book->id) }}" class="btn btn-outline-warning btn-rounded"><i class="far fa-edit"></i></a>
                            <a href="{{ route('delbook', $book->id) }}" class="btn btn-outline-red btn-rounded" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="text-center p-4 m-auto">
                <h1 class="pb-3 font-weight-bold">เพิ่มหนังสือเล่มแรกเลย!</h1>
                <a href="{{route('addbook')}}">
                    <button class="btn btn-lg btn-red font-weight-bold p-3 pl-5 pr-5"><i class="fas fa-plus"></i> เพิ่มหนังสือ</button>
                </a>
            </div>
        @endif
    </div>
@endsection
