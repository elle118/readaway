@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($trades as $trade)
            <div class="row overflow-auto">
                <div class="col mt-4 mb-4 float-left">
                    <h2>
                        ID : {{$trade->id}}
                        <a class="btn btn-red float-right" href="{{ route('mytrade') }}">
                            <i class="fas fa-chevron-left"></i> ย้อนกลับ
                        </a>
                    </h2>
                </div>
            </div>
            <div class="bg-white rounded shadow-sm">
                <div class="row">
                    <div class="col-auto">
                        <img class="preview-img-2" src="/{{$trade->trade_image}}">
                    </div>
                    <div class="col pt-3 position-relative pr-5">
                        <h2>{{$trade->book_name}}</h2>
                        <p>
                        <span class="text-black-50">ISBN : {{$trade->trade_isbn}}<br>{{$trade->created_at}}</span>
                        </p>
                        <p>สถานะ :
                            @if($trade->trade_status == 'incomplete')<span class="text-red">
                            @else<span class="text-success">@endif
                                {{$trade->trade_status}}
                            </span>
                        </p>
                        <p class="text-font">{{$trade->trade_caption}}</p>
                        @if($trade->trade_status == 'complete')
                            @foreach($offers as $offer)
                                <p class="position-absolute pos-bottom-left">
                                    หนังสือที่แลก : <span class="text-success">{{$offer->book_name}}</span><br>
                                    ผู้แลกเปลี่ยน : <span class="text-success">{{$offer->user_name}}</span>
                                </p>
                                <div class="position-absolute pos-bottom-right">
                                    <button class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i> ลบ
                                    </button>
                                    <button class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-edit"></i> แก้ไข
                                    </button>
                                </div>
                            @endforeach
                        @else
                            <p class="position-absolute pos-bottom-left">
                                หนังสือที่แลก : <span class="text-red">incomplete</span><br>
                                ผู้แลกเปลี่ยน : <span class="text-red">incomplete</span>
                            </p>
                            <div class="position-absolute pos-bottom-right">
                                <a href="{{ route('deltrade', $trade->id) }}" class="btn btn-outline-red" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                                    <i class="far fa-trash-alt"></i> ลบ
                                </a>
                                <a href="{{ route('editmytrade', $trade->id) }}" class="btn btn-outline-warning">
                                    <i class="far fa-edit"></i> แก้ไข
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row overflow-auto">
                <div class="col mt-4 mb-4">
                </div>
            </div>
            @if(count($offers) > 0)
                @if($trade->offer_id == 0)
                    <div class="row row-cols-1 row-cols-md-3">
                        @foreach($offers as $offer)
                            <div class="col mb-4">
                                <div class="card shadow-sm">
                                    <div class="card-body pb-2">
                                        <h5 class="card-title m-0">{{$offer->book_name}}</h5>
                                        <span class="text-black-50">{{$offer->user_name}}</span>
                                    </div>
                                    <img src="/{{$offer->offer_image}}" class="object-cover" height="200" alt="{{$trade->book_name}}">
                                    <div class="card-body">
                                        <span class="text-black-50">{{$offer->created_at}}</span>
                                        <p class="pt-3">{{$offer->offer_message}}</p>
                                        <div class="float-right overflow-auto">
                                            <a class="float-left mr-2" href="{{ route('deloffer', $offer->id) }}" onclick="return confirm('ยืนยันการปฏิเสธ?')">
                                                <button class="btn btn-outline-red">ปฏิเสธ</button>
                                            </a>
                                            <form class="float-left" action="{{ route('submitoffer', $trade->id) }}" onclick="return confirm('ยอมรับการแลกเปลี่ยนนี้?')">
                                                <button class="btn btn-red" name="offer_id" value="{{$offer->id}}">ยอมรับ</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                @endif
            @else
                <div class="text-center p-4 m-auto">
                    <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
                </div>
            @endif
        @endforeach
    </div>
@endsection
