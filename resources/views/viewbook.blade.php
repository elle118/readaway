@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row overflow-auto">
            <div class="col mt-4 mb-4 float-left">
                <h2>
                    ISBN : {{$book->isbn}}
                    <a class="btn btn-red float-right" href="{{ route('home') }}">
                        <i class="fas fa-chevron-left"></i> ย้อนกลับ
                    </a>
                </h2>
            </div>
        </div>
        <div class="bg-white rounded shadow-sm">
            <div class="row">
                <div class="col-auto">
                    <img class="preview-img-2" src="/{{$book->book_image}}">
                </div>
                <div class="col pt-3 position-relative pr-5">
                    <h2>{{$book->book_name}}</h2>
                    <p class="text-red pb-3">{{$book->writer}}</p>
                    <p class="text-font">{{$book->book_logline}}</p>
                </div>
            </div>
        </div>
        <div class="row overflow-auto">
            <div class="col mt-4 mb-4">
            </div>
        </div>
        @if(count($trades) > 0)
            <div class="row row-cols-1 row-cols-md-3">
                @foreach($trades as $trade)
                    <div class="col mb-4">
                        <div class="card shadow-sm">
                            <div class="card-body pb-2">
                                <h5 class="card-title m-0">{{$trade->name}}</h5>
                                <span class="text-black-50">{{$trade->created_at}}</span>
                                <p class="pt-3">{{$trade->trade_caption}}</p>
                            </div>
                            <img src="/{{$trade->trade_image}}" class="object-cover" height="200" alt="{{$trade->book_name}}">
                            <div class="card-body">
                                <div class="float-right">
                                    @if($trade->trade_user_id != auth()->user()->id)
                                        <a href="{{ route('addoffer', $trade->id) }}">
                                            <button class="btn btn-red">แลกเปลี่ยน</button>
                                        </a>
                                    @else
                                        <button class="btn btn-outline-red" onclick="return alert('ไม่สามารถแลกเปลี่ยนรายการตัวเองได้')">แลกเปลี่ยน</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="text-center p-4 m-auto">
                <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
            </div>
        @endif
    </div>
@endsection
