@extends('layouts.app')

@section('content')
<div class="container">
    <div class="">
        <button id="btn-book-list" class="btn btn-lg btn-red">หนังสือทั้งหมด</button>
        <button id="btn-trade-list" class="btn btn-lg">รายการแลกเปลี่ยนทั้งหมด</button>
    </div>
    @if(count($books) > 0)
        <div id="book-list">
            <div class="mt-4 mb-4 overflow-auto">
                <h2 class="float-left m-1">
                    รายการหนังสือ
                </h2>
                <form method="get" action="{{ route('search') }}" class="m-1">
                    <div class="input-group mb-3 w-50 float-right position-relative" >
                        <input type="text" class="form-control rounded-50 typeahead" placeholder="ค้นหาหนังสือ" name="searchname" id="searchname">
                        <button class="btn p-0 position-absolute pos-br" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
                @foreach($books as $book)
                    <a class="text-decoration-none text-dark" href="{{ route('viewbook', $book->id) }}">
                        <div class="col mb-4">
                            <div class="card shadow-sm">
                                <img src="{{$book->book_image}}" class="card-img-top object-cover" height="200" alt="{{$book->book_name}}">
                                <div class="card-body">
                                    <h5 class="card-title">{{$book->book_name}}</h5>
                                    <p class="card-text text-red">{{$book->writer}}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
        <div id="trade-list">
            <div class="mt-4 mb-4 overflow-auto">
                <h2 class="float-left m-1">
                    รายการแลกเปลี่ยน
                </h2>
                <form method="get" action="{{ route('search') }}" class="m-1">
                    <div class="input-group mb-3 w-50 float-right position-relative" >
                        <input type="text" class="form-control rounded-50 typeahead" placeholder="ค้นหาหนังสือ" name="searchname" id="searchname">
                        <button class="btn p-0 position-absolute pos-br" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
            @if(count($trades) > 0)
                <div class="row row-cols-1 row-cols-md-3">
                    @foreach($trades as $trade)
                        <div class="col mb-4">
                            <div class="card shadow-sm">
                                <div class="card-body pb-2">
                                    <h5 class="card-title m-0">{{$trade->name}}</h5>
                                    <span class="text-black-50">{{$trade->created_at}}</span>
                                    <p class="pt-3">{{$trade->trade_caption}}</p>
                                </div>
                                <img src="{{$trade->trade_image}}" class="object-cover" height="200" alt="{{$trade->book_name}}">
                                <div class="card-body">
                                    <p class="card-text">{{$trade->book_name}}</p>
                                    <div class="float-right">
                                        @if($trade->trade_user_id != auth()->user()->id)
                                            <a href="{{ route('addoffer', $trade->id) }}">
                                                <button class="btn btn-red">แลกเปลี่ยน</button>
                                            </a>
                                        @else
                                            <button class="btn btn-outline-red" onclick="return alert('ไม่สามารถแลกเปลี่ยนรายการตัวเองได้')">แลกเปลี่ยน</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="text-center p-4 m-auto">
                    <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
                    <h1 class="pb-3 font-weight-bold">เริ่มเป็นคนแรกเลย!</h1>
                    <a href="{{route('addtrade')}}">
                        <button class="btn btn-lg btn-outline-red font-weight-bold p-3 pl-5 pr-5"><i class="fas fa-plus"></i> เพิ่มรายการ</button>
                    </a>
                </div>
            @endif
        </div>
    @else
        <div class="text-center p-5 m-5">
            <h1 class="pb-3 font-weight-bold">ยังไม่มีหนังสือในระบบ :(</h1>
        </div>
    @endif
</div>
    <script>
        $("#trade-list").hide();
        $("#btn-book-list").click(function(){
            $("#btn-book-list").addClass('btn-red');
            $("#btn-trade-list").removeClass('btn-red');
            $("#book-list").show();
            $("#trade-list").hide();
        });
        $("#btn-trade-list").click(function(){
            $("#btn-book-list").removeClass('btn-red');
            $("#btn-trade-list").addClass('btn-red');
            $("#book-list").hide();
            $("#trade-list").show();
        });
    </script>
@endsection
