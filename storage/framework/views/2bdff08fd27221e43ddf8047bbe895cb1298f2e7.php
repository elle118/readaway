<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row overflow-auto">
            <div class="col mt-4 mb-4 float-left">
                <h2>
                    ISBN : <?php echo e($book->isbn); ?>

                    <a class="btn btn-red float-right" href="<?php echo e(route('home')); ?>">
                        <i class="fas fa-chevron-left"></i> ย้อนกลับ
                    </a>
                </h2>
            </div>
        </div>
        <div class="bg-white rounded shadow-sm">
            <div class="row">
                <div class="col-auto">
                    <img class="preview-img-2" src="/<?php echo e($book->book_image); ?>">
                </div>
                <div class="col pt-3 position-relative pr-5">
                    <h2><?php echo e($book->book_name); ?></h2>
                    <p class="text-red pb-3"><?php echo e($book->writer); ?></p>
                    <p class="text-font"><?php echo e($book->book_logline); ?></p>
                </div>
            </div>
        </div>
        <div class="row overflow-auto">
            <div class="col mt-4 mb-4">
            </div>
        </div>
        <?php if(count($trades) > 0): ?>
            <div class="row row-cols-1 row-cols-md-3">
                <?php $__currentLoopData = $trades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col mb-4">
                        <div class="card shadow-sm">
                            <div class="card-body pb-2">
                                <h5 class="card-title m-0"><?php echo e($trade->name); ?></h5>
                                <span class="text-black-50"><?php echo e($trade->created_at); ?></span>
                                <p class="pt-3"><?php echo e($trade->trade_caption); ?></p>
                            </div>
                            <img src="/<?php echo e($trade->trade_image); ?>" class="object-cover" height="200" alt="<?php echo e($trade->book_name); ?>">
                            <div class="card-body">
                                <div class="float-right">
                                    <?php if($trade->trade_user_id != auth()->user()->id): ?>
                                        <a href="<?php echo e(route('addoffer', $trade->id)); ?>">
                                            <button class="btn btn-red">แลกเปลี่ยน</button>
                                        </a>
                                    <?php else: ?>
                                        <button class="btn btn-outline-red" onclick="return alert('ไม่สามารถแลกเปลี่ยนรายการตัวเองได้')">แลกเปลี่ยน</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php else: ?>
            <div class="text-center p-4 m-auto">
                <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
            </div>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/viewbook.blade.php ENDPATH**/ ?>