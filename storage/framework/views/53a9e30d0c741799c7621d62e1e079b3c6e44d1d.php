<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="mt-4 mb-4">
            <h2 class="m-1">
                รายการแลกเปลี่ยนของคุณ
            </h2>
        </div>
        <?php if(count($trades) > 0): ?>
            <div class="row row-cols-1 row-cols-md-3">
                <?php $__currentLoopData = $trades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col mb-4">
                        <div class="card shadow-sm">
                            <div class="card-body pb-2">
                                <a class="text-dark" href="<?php echo e(route('viewmytrade', $trade->id)); ?>">
                                    <h5 class="card-title m-0"><?php echo e($trade->book_name); ?></h5>
                                </a>
                                <span class="text-black-50">ISBN : <?php echo e($trade->trade_isbn); ?></span>
                            </div>
                            <img src="<?php echo e($trade->trade_image); ?>" class="object-cover" height="200" alt="<?php echo e($trade->book_name); ?>">
                            <div class="card-body">
                                <?php if($trade->trade_status == 'incomplete'): ?>
                                    <span class="text-black-50"><?php echo e($trade->created_at); ?>

                                        <p class="card-text text-red"><?php echo e($trade->trade_status); ?></p>
                                    </span>
                                    <p class="pt-3 pb-4"><?php echo e($trade->trade_caption); ?></p>
                                    <div class="float-right">
                                        <a href="<?php echo e(route('delmytrade', $trade->id)); ?>" class="btn btn-outline-red" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                                            <i class="far fa-trash-alt"></i> ลบ
                                        </a>
                                        <a href="<?php echo e(route('editmytrade', $trade->id)); ?>" class="btn btn-outline-warning">
                                            <i class="far fa-edit"></i> แก้ไข
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <span class="text-black-50"><?php echo e($trade->created_at); ?>

                                        <p class="card-text text-success"><?php echo e($trade->trade_status); ?></p>
                                    </span>
                                    <p class="pt-3 pb-4"><?php echo e($trade->trade_caption); ?></p>
                                    <div class="float-right">
                                        <button class="btn btn-outline-secondary" disabled>
                                            <i class="far fa-trash-alt"></i> ลบ
                                        </button>
                                        <button class="btn btn-outline-secondary" disabled>
                                            <i class="far fa-edit"></i> แก้ไข
                                        </button>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php else: ?>
            <div class="text-center p-4 m-auto">
                <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
                <a href="<?php echo e(route('addbook')); ?>">
                    <button class="btn btn-lg btn-outline-red font-weight-bold p-3 pl-5 pr-5"><i class="fas fa-plus"></i> เพิ่มรายการแรก</button>
                </a>
            </div>
            <div class="text-center p-5 m-5">
            </div>
        <?php endif; ?>
    </div>
    <script>
        $("#trade-list").hide();
        $("#btn-book-list").click(function(){
            $("#btn-book-list").addClass('btn-red');
            $("#btn-trade-list").removeClass('btn-red');
            $("#book-list").show();
            $("#trade-list").hide();
        });
        $("#btn-trade-list").click(function(){
            $("#btn-book-list").removeClass('btn-red');
            $("#btn-trade-list").addClass('btn-red');
            $("#book-list").hide();
            $("#trade-list").show();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/trade.blade.php ENDPATH**/ ?>