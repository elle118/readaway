<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php $__currentLoopData = $trades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="row overflow-auto">
            <div class="col mt-4 mb-4 float-left">
                <h2>
                    ID : <?php echo e($trade->id); ?>

                    <a class="btn btn-red float-right" href="<?php echo e(route('tradelist')); ?>">
                        <i class="fas fa-chevron-left"></i> ย้อนกลับ
                    </a>
                </h2>
            </div>
        </div>
        <div class="bg-white rounded shadow-sm">
            <div class="row">
                <div class="col-auto">
                    <img class="preview-img-2" src="/<?php echo e($trade->trade_image); ?>">
                </div>
                <div class="col pt-3 position-relative pr-5">
                    <h2><?php echo e($trade->name); ?></h2>
                    <p>
                        <h5 class="p-0 m-0"><?php echo e($trade->book_name); ?></h5>
                        <span class="text-black-50">ISBN : <?php echo e($trade->trade_isbn); ?><br><?php echo e($trade->created_at); ?></span>
                    </p>
                    <p>สถานะ :
                    <?php if($trade->trade_status == 'incomplete'): ?><span class="text-red">
                    <?php else: ?><span class="text-success"><?php endif; ?>
                        <?php echo e($trade->trade_status); ?></span>
                    </p>
                    <p class="text-font"><?php echo e($trade->trade_caption); ?></p>
                    <?php if($trade->offer_id != 0): ?>
                        <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if($trade->offer_id == $offer->id): ?>
                                <p class="position-absolute pos-bottom-left">
                                    หนังสือที่แลก : <span class="text-success"><?php echo e($offer->book_name); ?></span><br>
                                    ผู้แลกเปลี่ยน : <span class="text-success"><?php echo e($offer->user_name); ?></span>
                                </p>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <p class="position-absolute pos-bottom-left">
                            หนังสือที่แลก : <span class="text-red">incomplete</span><br>
                            ผู้แลกเปลี่ยน : <span class="text-red">incomplete</span>
                        </p>
                    <?php endif; ?>
                    <div class="position-absolute pos-bottom-right">
                        <a href="<?php echo e(route('deltrade', $trade->id)); ?>" class="btn btn-outline-red" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                            <i class="far fa-trash-alt"></i> ลบ
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/admin/tradedetail.blade.php ENDPATH**/ ?>