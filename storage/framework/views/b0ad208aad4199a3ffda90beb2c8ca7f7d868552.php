<?php $__env->startSection('content'); ?>
    <div class="container">
        <h2 class="mt-3">แก้ไขหนังสือ</h2>
        <form method="post" action="<?php echo e(route('updatebook', $book->id)); ?>" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="card-body bg-white rounded shadow-sm mt-3 pt-5 pb-5 row-cols-2 overflow-auto">
                <div class="col float-left">
                    <div class="form-group mb-4">
                        <p class="font-weight-bold">รหัส ISBN <span class="text-red">*</span></p>
                        <input class="form-control rounded-50" type="text" name="isbn" value="<?php echo e($book->isbn); ?>">
                    </div>
                    <div class="form-group mb-4">
                        <p class="font-weight-bold">ชื่อหนังสือ <span class="text-red">*</span></p>
                        <input class="form-control rounded-50" type="text" name="book_name" value="<?php echo e($book->book_name); ?>">
                    </div>
                    <div class="form-group mb-4">
                        <p class="font-weight-bold">ผู้เขียน <span class="text-red">*</span></p>
                        <input class="form-control rounded-50" type="text" name="writer" value="<?php echo e($book->writer); ?>">
                    </div>
                    <div class="form-group mb-4">
                        <p class="font-weight-bold">คำโปรย <span class="text-red">*</span></p>
                        <textarea class="form-control rounded-nm" name="book_logline" cols="30" rows="5"><?php echo e($book->book_logline); ?></textarea>
                    </div>
                </div>
                <div class="col float-left">
                    <p class="font-weight-bold">รูปภาพ <span class="text-red">*</span></p>
                    <div class="custom-file rounded-50">
                        <input type="file" class="custom-file-input" id="customFile" name="book_image">
                        <label class="custom-file-label rounded-50" for="customFile">Choose new image</label>
                        <img id="blah" src="/<?php echo e($book->book_image); ?>" alt="book image" class="preview-img mt-3 rounded-nm"/>
                    </div>
                </div>
            </div>
            <a class="btn btn-outline-red mt-3" href="<?php echo e(redirect()->back()); ?>">ยกเลิก</a>
            <button class="btn btn-red mt-3" type="submit" onclick="return confirm('ยืนยันการแก้ไข?')">ยืนยัน</button>
        </form>

    </div>
    <script>
        document.title = 'Books | Add';

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').show();
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#customFile").change(function() {
            readURL(this);
        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/admin/editbook.blade.php ENDPATH**/ ?>