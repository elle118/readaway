<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row overflow-auto">
            <div class="col mt-4 mb-4 float-left">
                <h2>
                    ISBN : <?php echo e($book->isbn); ?>

                    <a class="btn btn-red float-right" href="<?php echo e(route('booklist')); ?>">
                        <i class="fas fa-chevron-left"></i> ย้อนกลับ
                    </a>
                </h2>
            </div>
        </div>
        <div class="bg-white rounded shadow-sm">
            <div class="row">
                <div class="col-auto">
                    <img class="preview-img-2" src="/<?php echo e($book->book_image); ?>">
                </div>
                <div class="col pt-3 position-relative pr-5">
                    <h2><?php echo e($book->book_name); ?></h2>
                    <p class="text-red pb-3"><?php echo e($book->writer); ?></p>
                    <p class="text-font"><?php echo e($book->book_logline); ?></p>
                    <div class="position-absolute pos-bottom-right">
                        <a href="<?php echo e(route('delbook', $book->id)); ?>" class="btn btn-outline-red" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                            <i class="far fa-trash-alt"></i> ลบ
                        </a>
                        <a href="<?php echo e(route('editbook', $book->id)); ?>" class="btn btn-outline-warning">
                            <i class="far fa-edit"></i> แก้ไข
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/admin/bookdetail.blade.php ENDPATH**/ ?>