<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Happy+Monkey&family=Kanit:wght@200;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=K2D&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse position-relative" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <?php if(auth()->guard()->guest()): ?>
                            <a class="navbar-brand logo-font font-weight-bold" href="<?php echo e(url('/')); ?>">
                                read<span class="text-red logo-font"> away</span>
                            </a>
                        <?php else: ?>
                            <?php if(Auth::user()->isAdmin()): ?>
                                <a class="btn mr-2" href="<?php echo e(route('booklist')); ?>"><i class="fas fa-list-ul"></i> รายการหนังสือ</a>
                                <a class="btn" href="<?php echo e(route('tradelist')); ?>"><i class="fas fa-retweet"></i> รายการแลกเปลี่ยน</a>
                            <?php else: ?>
                                <div id="mySidenav" class="sidenav shadow-sm">
                                    <label class="text-red"><i class="fas fa-at"></i> <?php echo e(Auth::user()->name); ?></label>
                                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                    <a class="dropdown-item" href="<?php echo e(route('home')); ?>"><i class="fas fa-home"></i> หน้าแรก</a>
                                    <a class="dropdown-item" href="<?php echo e(route('mytrade')); ?>"><i class="fas fa-list-ul"></i> รายการของคุณ</a>
                                    <a class="dropdown-item" href="<?php echo e(route('myoffer')); ?>"><i class="fas fa-retweet"></i> คำขอของคุณ</a>
                                    <a class="dropdown-item" href="#"><i class="fas fa-history"></i> ประวัติการค้นหา</a>
                                    <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="return logout(event);">
                                        <i class="fas fa-sign-out-alt"></i> ออกจากระบบ
                                    </a>
                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="post" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>

                                <!-- Use any element to open the sidenav -->
                                <h4 onclick="openNav()" class="m-0"><i class="fas fa-bars text-red"></i></h4>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>

                    <!-- Center Side Of Navbar -->
                    <ul class="navbar-nav position-absolute" style="left: 47%;">
                        <?php if(auth()->guard()->guest()): ?>
                        <?php else: ?>
                            <a class="navbar-brand logo-font font-weight-bold m-0" href="<?php echo e(url('/')); ?>">
                                read<span class="text-red logo-font"> away</span>
                            </a>
                        <?php endif; ?>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <?php if(auth()->guard()->guest()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                            </li>
                            <?php if(Route::has('register')): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                                </li>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php if(Auth::user()->isAdmin()): ?>
                                <label class="m-auto pr-3 font-weight-bold"><?php echo e(Auth::user()->name); ?></label>
                                <a class="btn btn-outline-red" href="<?php echo e(route('logout')); ?>" onclick="return logout(event);">
                                    <i class="fas fa-sign-out-alt"></i> ออกจากระบบ
                                </a>
                                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="post" style="display: none;">
                                    <?php echo csrf_field(); ?>
                                </form>
                            <?php else: ?>
                                <li class="nav-item dropdown">
                                    <a href="<?php echo e(route('addtrade')); ?>" class="btn btn-red"><i class="fas fa-plus"></i> เพิ่มรายการ</a>
                                </li>
                            <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <?php echo $__env->make('inc.message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>
        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>

    <script src="https://kit.fontawesome.com/e73e3b4d8f.js" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/8c491831a5.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <script src="<?php echo e(asset('js/app.js')); ?>"></script>

</body>
</html>
<?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/layouts/app.blade.php ENDPATH**/ ?>