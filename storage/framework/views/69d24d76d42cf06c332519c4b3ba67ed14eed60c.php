<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(count($trades) > 0): ?>
            <div class="mt-4 mb-4">
                <h2>
                    รายการแลกเปลี่ยน <span class="text-red" style="font-size: 16px;">ทั้งหมด <?php echo e(count($trades)); ?> รายการ</span>
                </h2>
            </div>
            <div class="card-body bg-white rounded shadow-sm overflow-auto pr-5 pl-5">
                <div class="row">
                    <div class="col-sm-1 m-auto">ID</div>
                    <div class="col m-auto">ISBN</div>
                    <div class="col m-auto">ชื่อหนังสือ</div>
                    <div class="col m-auto">เจ้าของ</div>
                    <div class="col m-auto">สถานะ</div>
                    <div class="col-sm-1 m-auto">Action</div>
                </div><hr>
                <?php $__currentLoopData = $trades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="row pt-3 pb-3">
                        <a class="col-sm-1 m-auto text-red font-weight-bold" href="<?php echo e(route('showtrade', $trade->id)); ?>">
                            <?php echo e($trade->id); ?>

                        </a>
                        <div class="col m-auto"><?php echo e($trade->trade_isbn); ?></div>
                        <div class="col m-auto"><?php echo e($trade->book_name); ?></div>
                        <div class="col m-auto"><?php echo e($trade->name); ?></div>
                        <?php if($trade->trade_status == 'incomplete'): ?>
                            <div class="col m-auto text-red"><?php echo e($trade->trade_status); ?></div>
                        <?php else: ?>
                            <div class="col m-auto text-success"><?php echo e($trade->trade_status); ?></div>
                        <?php endif; ?>

                        <div class="col-sm-1 m-auto">
                            <a href="<?php echo e(route('deltrade', $trade->id)); ?>" class="btn btn-outline-red btn-rounded" onclick="return confirm('ยืนยันการลบรายการแลกเปลี่ยน?');">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php else: ?>
            <div class="text-center p-5 m-5">
                <h1 class="pb-3 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน :(</h1>
            </div>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/admin/tradelist.blade.php ENDPATH**/ ?>