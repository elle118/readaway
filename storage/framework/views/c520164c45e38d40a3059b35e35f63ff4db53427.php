<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="">
        <button id="btn-book-list" class="btn btn-lg btn-red">หนังสือทั้งหมด</button>
        <button id="btn-trade-list" class="btn btn-lg">รายการแลกเปลี่ยนทั้งหมด</button>
    </div>
    <?php if(count($books) > 0): ?>
        <div id="book-list">
            <div class="mt-4 mb-4 overflow-auto">
                <h2 class="float-left m-1">
                    รายการหนังสือ
                </h2>
                <form method="get" action="<?php echo e(route('search')); ?>" class="m-1">
                    <div class="input-group mb-3 w-50 float-right position-relative" >
                        <input type="text" class="form-control rounded-50 typeahead" placeholder="ค้นหาหนังสือ" name="searchname" id="searchname">
                        <button class="btn p-0 position-absolute pos-br" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
            <div class="row row-cols-1 row-cols-md-3">
                <?php $__currentLoopData = $books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a class="text-decoration-none text-dark" href="<?php echo e(route('viewbook', $book->id)); ?>">
                        <div class="col mb-4">
                            <div class="card shadow-sm">
                                <img src="<?php echo e($book->book_image); ?>" class="card-img-top object-cover" height="200" alt="<?php echo e($book->book_name); ?>">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo e($book->book_name); ?></h5>
                                    <p class="card-text text-red"><?php echo e($book->writer); ?></p>
                                </div>
                            </div>
                        </div>
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div id="trade-list">
            <div class="mt-4 mb-4 overflow-auto">
                <h2 class="float-left m-1">
                    รายการแลกเปลี่ยน
                </h2>
                <form method="get" action="<?php echo e(route('search')); ?>" class="m-1">
                    <div class="input-group mb-3 w-50 float-right position-relative" >
                        <input type="text" class="form-control rounded-50 typeahead" placeholder="ค้นหาหนังสือ" name="searchname" id="searchname">
                        <button class="btn p-0 position-absolute pos-br" type="submit"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
            <?php if(count($trades) > 0): ?>
                <div class="row row-cols-1 row-cols-md-3">
                    <?php $__currentLoopData = $trades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col mb-4">
                            <div class="card shadow-sm">
                                <div class="card-body pb-2">
                                    <h5 class="card-title m-0"><?php echo e($trade->name); ?></h5>
                                    <span class="text-black-50"><?php echo e($trade->created_at); ?></span>
                                    <p class="pt-3"><?php echo e($trade->trade_caption); ?></p>
                                </div>
                                <img src="<?php echo e($trade->trade_image); ?>" class="object-cover" height="200" alt="<?php echo e($trade->book_name); ?>">
                                <div class="card-body">
                                    <p class="card-text"><?php echo e($trade->book_name); ?></p>
                                    <div class="float-right">
                                        <?php if($trade->trade_user_id != auth()->user()->id): ?>
                                            <a href="<?php echo e(route('addoffer', $trade->id)); ?>">
                                                <button class="btn btn-red">แลกเปลี่ยน</button>
                                            </a>
                                        <?php else: ?>
                                            <button class="btn btn-outline-red" onclick="return alert('ไม่สามารถแลกเปลี่ยนรายการตัวเองได้')">แลกเปลี่ยน</button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            <?php else: ?>
                <div class="text-center p-4 m-auto">
                    <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
                    <h1 class="pb-3 font-weight-bold">เริ่มเป็นคนแรกเลย!</h1>
                    <a href="<?php echo e(route('addtrade')); ?>">
                        <button class="btn btn-lg btn-outline-red font-weight-bold p-3 pl-5 pr-5"><i class="fas fa-plus"></i> เพิ่มรายการ</button>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    <?php else: ?>
        <div class="text-center p-5 m-5">
            <h1 class="pb-3 font-weight-bold">ยังไม่มีหนังสือในระบบ :(</h1>
        </div>
    <?php endif; ?>
</div>
    <script>
        $("#trade-list").hide();
        $("#btn-book-list").click(function(){
            $("#btn-book-list").addClass('btn-red');
            $("#btn-trade-list").removeClass('btn-red');
            $("#book-list").show();
            $("#trade-list").hide();
        });
        $("#btn-trade-list").click(function(){
            $("#btn-book-list").removeClass('btn-red');
            $("#btn-trade-list").addClass('btn-red');
            $("#book-list").hide();
            $("#trade-list").show();
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/home.blade.php ENDPATH**/ ?>