<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php $__currentLoopData = $trades; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $trade): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="row overflow-auto">
                <div class="col mt-4 mb-4 float-left">
                    <h2>
                        ID : <?php echo e($trade->id); ?>

                        <a class="btn btn-red float-right" href="<?php echo e(route('mytrade')); ?>">
                            <i class="fas fa-chevron-left"></i> ย้อนกลับ
                        </a>
                    </h2>
                </div>
            </div>
            <div class="bg-white rounded shadow-sm">
                <div class="row">
                    <div class="col-auto">
                        <img class="preview-img-2" src="/<?php echo e($trade->trade_image); ?>">
                    </div>
                    <div class="col pt-3 position-relative pr-5">
                        <h2><?php echo e($trade->book_name); ?></h2>
                        <p>
                        <span class="text-black-50">ISBN : <?php echo e($trade->trade_isbn); ?><br><?php echo e($trade->created_at); ?></span>
                        </p>
                        <p>สถานะ :
                            <?php if($trade->trade_status == 'incomplete'): ?><span class="text-red">
                            <?php else: ?><span class="text-success"><?php endif; ?>
                                <?php echo e($trade->trade_status); ?>

                            </span>
                        </p>
                        <p class="text-font"><?php echo e($trade->trade_caption); ?></p>
                        <?php if($trade->trade_status == 'complete'): ?>
                            <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <p class="position-absolute pos-bottom-left">
                                    หนังสือที่แลก : <span class="text-success"><?php echo e($offer->book_name); ?></span><br>
                                    ผู้แลกเปลี่ยน : <span class="text-success"><?php echo e($offer->user_name); ?></span>
                                </p>
                                <div class="position-absolute pos-bottom-right">
                                    <button class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-trash-alt"></i> ลบ
                                    </button>
                                    <button class="btn btn-outline-secondary" disabled>
                                        <i class="far fa-edit"></i> แก้ไข
                                    </button>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php else: ?>
                            <p class="position-absolute pos-bottom-left">
                                หนังสือที่แลก : <span class="text-red">incomplete</span><br>
                                ผู้แลกเปลี่ยน : <span class="text-red">incomplete</span>
                            </p>
                            <div class="position-absolute pos-bottom-right">
                                <a href="<?php echo e(route('deltrade', $trade->id)); ?>" class="btn btn-outline-red" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                                    <i class="far fa-trash-alt"></i> ลบ
                                </a>
                                <a href="<?php echo e(route('editmytrade', $trade->id)); ?>" class="btn btn-outline-warning">
                                    <i class="far fa-edit"></i> แก้ไข
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row overflow-auto">
                <div class="col mt-4 mb-4">
                </div>
            </div>
            <?php if(count($offers) > 0): ?>
                <?php if($trade->offer_id == 0): ?>
                    <div class="row row-cols-1 row-cols-md-3">
                        <?php $__currentLoopData = $offers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $offer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col mb-4">
                                <div class="card shadow-sm">
                                    <div class="card-body pb-2">
                                        <h5 class="card-title m-0"><?php echo e($offer->book_name); ?></h5>
                                        <span class="text-black-50"><?php echo e($offer->user_name); ?></span>
                                    </div>
                                    <img src="/<?php echo e($offer->offer_image); ?>" class="object-cover" height="200" alt="<?php echo e($trade->book_name); ?>">
                                    <div class="card-body">
                                        <span class="text-black-50"><?php echo e($offer->created_at); ?></span>
                                        <p class="pt-3"><?php echo e($offer->offer_message); ?></p>
                                        <div class="float-right overflow-auto">
                                            <a class="float-left mr-2" href="<?php echo e(route('deloffer', $offer->id)); ?>" onclick="return confirm('ยืนยันการปฏิเสธ?')">
                                                <button class="btn btn-outline-red">ปฏิเสธ</button>
                                            </a>
                                            <form class="float-left" action="<?php echo e(route('submitoffer', $trade->id)); ?>" onclick="return confirm('ยอมรับการแลกเปลี่ยนนี้?')">
                                                <button class="btn btn-red" name="offer_id" value="<?php echo e($offer->id); ?>">ยอมรับ</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                <?php else: ?>
                <?php endif; ?>
            <?php else: ?>
                <div class="text-center p-4 m-auto">
                    <h5 class="p-5 font-weight-bold">ยังไม่มีรายการแลกเปลี่ยน</h5>
                </div>
            <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/viewmytrade.blade.php ENDPATH**/ ?>