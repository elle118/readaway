<?php $__env->startSection('content'); ?>
    <div class="container">
        <?php if(count($books) > 0): ?>
            <a href="<?php echo e(route('addbook')); ?>">
                <button id="addbook" class="btn btn-red p-0"><i class="fas fa-plus"></i></button>
            </a>
            <div class="mt-4 mb-4">
                <h2>
                    รายการหนังสือ <span class="text-red" style="font-size: 16px;">ทั้งหมด <?php echo e(count($books)); ?> เล่ม</span>
                </h2>
            </div>
            <div class="card-body bg-white rounded shadow-sm overflow-auto pr-5 pl-5">
                <div class="row">
                    <div class="col-sm-1 m-auto"></div>
                    <div class="col-md-2 m-auto">ISBN</div>
                    <div class="col m-auto">ชื่อหนังสือ</div>
                    <div class="col m-auto">นักเขียน</div>
                    <div class="col-sm-2 m-auto">Action</div>
                </div><hr>
                <?php $__currentLoopData = $books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="row pt-3 pb-3">
                        <div class="col-sm-1">
                            <img class="img-list rounded-lg mr-3" src="/<?php echo e($book->book_image); ?>" alt="<?php echo e($book->book_name); ?>">
                        </div>
                        <a class="col-md-2 m-auto text-dark font-weight-bold" href="<?php echo e(route('showbook', $book->id)); ?>">
                            <?php echo e($book->isbn); ?>

                        </a>
                        <div class="col m-auto"><?php echo e($book->book_name); ?></div>
                        <div class="col m-auto"><?php echo e($book->writer); ?></div>
                        <div class="col-sm-2 m-auto">
                            <a href="<?php echo e(route('editbook', $book->id)); ?>" class="btn btn-outline-warning btn-rounded"><i class="far fa-edit"></i></a>
                            <a href="<?php echo e(route('delbook', $book->id)); ?>" class="btn btn-outline-red btn-rounded" onclick="return confirm('ยืนยันการลบหนังสือ?');">
                                <i class="far fa-trash-alt"></i>
                            </a>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        <?php else: ?>
            <div class="text-center p-4 m-auto">
                <h1 class="pb-3 font-weight-bold">เพิ่มหนังสือเล่มแรกเลย!</h1>
                <a href="<?php echo e(route('addbook')); ?>">
                    <button class="btn btn-lg btn-red font-weight-bold p-3 pl-5 pr-5"><i class="fas fa-plus"></i> เพิ่มหนังสือ</button>
                </a>
            </div>
        <?php endif; ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/admin/booklist.blade.php ENDPATH**/ ?>