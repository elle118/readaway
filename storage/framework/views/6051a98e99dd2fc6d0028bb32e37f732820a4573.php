<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Happy+Monkey&family=Kanit:wght@200;400&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <?php if(auth()->guard()->guest()): ?>
                        <?php else: ?>
                            <a class="btn text-red" href="<?php echo e(route('booklist')); ?>"><i class="fas fa-list-ul"></i> รายการหนังสือ</a>
                            <a class="btn text-red" href="<?php echo e(route('request')); ?>"><i class="fas fa-retweet"></i> รายการแลกเปลี่ยน</a>
                        <?php endif; ?>
                    </ul>

                    <!-- Center Side Of Navbar -->
                    <ul class="navbar-nav">
                        <?php if(auth()->guard()->guest()): ?>
                        <?php else: ?>
                            <a class="navbar-brand logo-font font-weight-bold m-0" href="<?php echo e(url('/')); ?>">
                                read<span class="text-red logo-font"> away</span>
                            </a>
                        <?php endif; ?>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <?php if(auth()->guard()->guest()): ?>
                        <?php else: ?>
                            <label class="m-auto pr-3">Admin : <?php echo e(Auth::user()->name); ?></label>
                            <a class="btn btn-outline-red" href="<?php echo e(route('logout')); ?>" onclick="return logout(event);">
                                <i class="fas fa-sign-out-alt"></i> ออกจากระบบ
                            </a>
                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="post" style="display: none;">
                                <?php echo csrf_field(); ?>
                            </form>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>

    <script src="https://kit.fontawesome.com/e73e3b4d8f.js" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/8c491831a5.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    <script src="<?php echo e(asset('js/app.js')); ?>"></script>

</body>
</html>
<?php /**PATH C:\xampp\htdocs\13600179_phpfnp\resources\views/layouts/admin.blade.php ENDPATH**/ ?>